//
//  ArchivingRepository.swift
//  PersonTabGit_PL2
//
//  Created by formando on 13/10/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import Foundation

class ArchivingRepository: RepositoryProtocol {
    
    //singleton repository
    static let repository = ArchivingRepository()
    
    fileprivate init() {}
    
    var people = [Person]()
    var documentsPath = URL(fileURLWithPath: "")
    var filePath = URL(fileURLWithPath: "")
    
    
    func loadPeople() {
        documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        filePath = documentsPath.appendingPathComponent("people.data", isDirectory: false)
        let path = filePath.path;
        if let newData = NSKeyedUnarchiver.unarchiveObject(withFile: path) as? [Person] {
            people = newData
        }
        
    }
    
    func savePeople() {
        documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        filePath = documentsPath.appendingPathComponent("people.data", isDirectory: false)
        let path = filePath.path;
        NSKeyedArchiver.archiveRootObject(people, toFile: path)
    }
}
