//
//  Person.swift
//  PersonTabGit_PL2
//
//  Created by Catarina Silva on 29/09/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import Foundation

class Person:NSObject, NSCoding {
    var firstName:String?
    var lastName:String?
    var nationality:String //= "Portuguese"
    
    override init() {
        self.nationality = "Portuguese"
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(firstName, forKey: "first")
        aCoder.encode(lastName, forKey: "last")
        aCoder.encode(nationality, forKey: "nat")
    }
    
    required init?(coder aDecoder: NSCoder) {
        firstName = aDecoder.decodeObject(forKey: "first") as? String
        lastName = aDecoder.decodeObject(forKey: "last") as? String
        let nat = aDecoder.decodeObject(forKey: "nat") as? String
        nationality = (nat ?? "Portuguesa")
        
    }
    
    init(firstName:String, lastName:String, nationality:String) {
        self.firstName = firstName
        self.lastName = lastName
        self.nationality = nationality
    }
    
    func fullName() -> String {
        return firstName! + " " + lastName!
    }
    
}





