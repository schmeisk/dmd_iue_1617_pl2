//
//  Country+JSON.swift
//  PersonTabGit_PL2
//
//  Created by formando on 17/10/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import Foundation
extension Country {
    static func parse(json: [String:Any]) -> Country? {
        guard let name = json["countryName"] as? String else {
            return nil
        }
        guard let capital = json["capital"] as? String else {
            return nil
        }
        guard let populationStr = json["population"] as? String else {
            return nil
        }
        let population = Int(populationStr) ?? 0
        let country = Country(name: name, population: population, capital: capital)
        
        return country
        
    }
}
