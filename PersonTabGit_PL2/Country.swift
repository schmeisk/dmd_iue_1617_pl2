//
//  Country.swift
//  PersonTabGit_PL2
//
//  Created by formando on 17/10/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import Foundation

struct Country {
    let name: String
    let population:Int
    let capital:String
}
