//
//  RepositoryProtocol.swift
//  PersonTabGit_PL2
//
//  Created by formando on 13/10/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import Foundation

protocol RepositoryProtocol {
    func savePeople()
    func loadPeople()
}
