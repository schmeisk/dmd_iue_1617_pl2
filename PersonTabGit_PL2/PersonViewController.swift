//
//  PersonViewController.swift
//  PersonTabGit_PL2
//
//  Created by Catarina Silva on 29/09/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import UIKit

class PersonViewController: UIViewController {

    @IBOutlet weak var labelText: UILabel!
    @IBOutlet weak var firstNameText: UITextField!
    @IBOutlet weak var lastNameText: UITextField!
    @IBOutlet weak var nationalityText: UITextField!
    @IBOutlet weak var txtFTest: UITextField!
    
    let defaults = UserDefaults.standard
    
    override func viewWillDisappear(_ animated: Bool) {
        defaults.set(txtFTest.text, forKey: "text")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let text = defaults.string(forKey: "text"){
            txtFTest.text = text
        }
    }
    
    var person : Person?

    @IBAction func saveAction(_ sender: AnyObject) {
        if let p = person {
            // person exists
            p.firstName = firstNameText.text
            p.lastName = lastNameText.text
            p.nationality = nationalityText.text!
            
            self.navigationController?.popViewController(animated: true)
        }
        else {
            let newPerson = Person(firstName: firstNameText.text!, lastName: lastNameText.text!, nationality: nationalityText.text!)
            ArchivingRepository.repository.people.append(newPerson)
            ArchivingRepository.repository.savePeople()
            self.navigationController?.dismiss(animated: true, completion: nil)
        }
    
    }
    
    
    @IBAction func setButtonPressed(_ sender: UIButton) {
/*
        person.firstName = firstNameText.text
        person.lastName = lastNameText.text
        if let nat = nationalityText.text {
            person.nationality = nat
        }
        //person.nationality = nationalityText.text
        labelText.text = person.fullName() + person.nationality
     
        print("nationality: /(person.nationality)")
 */
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.firstNameText.text = self.person?.firstName
        self.lastNameText.text = self.person?.lastName
        self.nationalityText.text = self.person?.nationality

        // Do any additional setup after loading the view.
        if person == nil {
            let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(PersonViewController.cancelAction))
            self.navigationItem.leftBarButtonItem = cancelButton
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func cancelAction(){
        self.navigationController?.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
